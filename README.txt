Publications for UCSF Profiles (ucsf_profiles)


ABOUT
-----
Publications for UCSF Profiles module is an integration module for the UCSF
Profiles publication service.

The module uses UCSF Profiles' API to query employee IDs. The API response is
then saved & cached locally (files directory) in JSON format. A menu callback
function dynamically generates a CSV file from the files stored in files
directory, which then integrated with Feeds module's importer.

It is an alternative solution to Biblio for any UCSF Drupal websites.

What is UCSF Profiles?
UCSF Profiles enables the discovery of research expertise at UCSF, allowing
for new ways to network and collaborate between researchers, between mentors
and mentees, between researchers and community or industry partners, and much
more.

Experts, Collaborators, Mentors
* 85,000+ publications
* 2400+ UCSF faculty
* 1000+ UCSF postdocs
* 25+ biomedical institutions nationwide
* Directory information
* Soon to include UCSF residents, fellows, research staff

Visit http://profiles.ucsf.edu/ for more information about UCSF Profiles
services.


DESCRIPTION
-----------
UCSF Profiles (ucsf_profiles) module uses UCSF Profile's API to query and
import publications of UCSF employees where their employee is entered in the
module settings page (admin/settings/ucsf_profiles).

UCSF Profiles integrates with Feeds modules to import publications as nodes.

Required Modules:
* Feeds <http://drupal.org/project/feeds>
* Email <http://drupal.org/project/email>
* Features <http://drupal.org/project/features>
* FileField <http://drupal.org/project/filefield>
* Link <http://drupal.org/project/link>
* CCK <http://drupal.org/project/cck>


IMPORTANT!
----------
ucsf_profiles 6.x-2.x branch is completely re-written and it is now the recommended branch. There will not be an upgrade path from 1.x branch.

You must uninstall 6.x-1.x if you already have, and remove the module directory completely before installing 2.x branch.

2.x Features:
- Now using JSON API, instead of XML API (http://profiles.ucsf.edu/)
- Module dependencies have changed.
- Storage method is now nodes. Imported publications are saved as nodes. With
  the help of Feeds module imported publication nodes will be able sync easily.
- Publication nodes use a custom content-type, which would let developers to
  easily extend its fields. Non-mapped fields will not be effected by
  the "refresh".


AUTHOR
------
Osman Gormus <osman@project6.com>
http://drupal.org/user/70527
Project6 Design, Inc.
