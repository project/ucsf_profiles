<?php

/**
 * Implementation of hook_node_info().
 */
function ucsf_profiles_publication_node_info() {
  $items = array(
    'ucsf_profiles_publication' => array(
      'name' => t('Publication'),
      'module' => 'features',
      'description' => t('<em>Publication</em>is used to automatically create publication nodes which pulled from <a href="http://profiles.ucsf.edu/">UCSF Profiles API</a>.'),
      'has_title' => '1',
      'title_label' => t('Publication ID'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
