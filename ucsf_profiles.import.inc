<?php
/**
 * @file
 * Batch API integration and import functions.
 */

/**
 * Callback for the batch operations.
 */
function ucsf_profiles_batch() {
  $employee_id = array_unique(preg_split("/[\s,]+/", variable_get('ucsf_profiles_ids', '')));

  $operations = array();

  if (is_array($employee_id)) {
    foreach ($employee_id as $id) {
      $id = str_pad($id, 9, "0", STR_PAD_LEFT);
      $operations[] = array('ucsf_profiles_save_as_json', array($id));
    }
  }

  $batch = array(
    'title' => t('Downloading publications from UCSF Profiles'),
    'operations' => $operations,
    'finished' => 'ucsf_profiles_batch_finished',
    'file' => drupal_get_path('module', 'ucsf_profiles') . '/ucsf_profiles.import.inc',
  );
  batch_set($batch);
  batch_process();
}

/**
 * Save JSON files to system files directory.
 */
function ucsf_profiles_save_as_json($employee_id, &$context) {
  global $base_url;
  static $sourceid = NULL;

  if ($employee_id) {
    if ($sourceid === NULL) {
      // "source" value for profiles.ucsf.edu so they can track use
      $sourceid = preg_replace('/[^\.a-z]/', '-', strtolower(rtrim(substr($base_url, strpos($base_url, '://')+3), '/')));
    }

    $person_id = (int) ($employee_id / 10) + 2569307;
    // JSON URI for the person queried.
    $json_source = 'http://api.profiles.ucsf.edu/json/v2/'
      . '?source=' . $sourceid
      . '&Person=' . $person_id
      . '&publications=full';

    // Define publications folder to save all JSON files in.
    $pubs_path = file_directory_path()
               . '/'
               . variable_get('ucsf_profiles_pubs_dir', 'ucsf_profiles_publications');

    $handle = '';
    $result = t('Could not retrieve JSON file for Employee ID# @employee_id', array('@employee_id' => $employee_id));
    $message = t('Could not download JSON file for Employee ID# @employee_id', array('@employee_id' => $employee_id));
    if (file_check_directory($pubs_path, 3)) {
      if ($handle = fopen($json_source, 'r')) {
        $message = t('Downloading JSON file for Employee ID# @employee_id', array('@employee_id' => $employee_id));
        if ($json = stream_get_contents($handle)) {

          // Save the JSON file. Replace if exists.
          if ($pubs_file = file_save_data($json, "{$pubs_path}/{$employee_id}_{$person_id}.json", FILE_EXISTS_REPLACE)) {
            $result = t('Retrieved JSON file for Employee ID# @employee_id', array('@employee_id' => $employee_id));
          }
        }
        fclose($handle);
      }
    }

    $context['results'][] = $result;
    $context['message'] = $message;
  }
}

/**
 * Finished callback of import batch.
 *
 * Reports the number of XML files that processed, and the number of
 * publications retrieved for each person queried, or an error message
 * explaining what went wrong.
 *
 * @param bool $success
 *   Boolean.
 *
 * @param array $results
 *   The array of results gathered so far by the batch processing.
 *
 * @param array $operations
 *   Callback functions defined by the batch.
 */
function ucsf_profiles_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'Downloaded one JSON file.', 'Downloaded @count JSON files.');
    variable_set('ucsf_profiles_cron_last', $_SERVER['REQUEST_TIME']);
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while processing ' . $error_operation[0] . ' with arguments: ' . print_r($error_operation[0], TRUE);
  }
  drupal_set_message(filter_xss(t('!results', array('!results' => $message))));
}
