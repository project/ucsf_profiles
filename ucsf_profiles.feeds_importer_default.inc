<?php
/**
 * @file
 * Creates a Feed importers for pulling UCSF Profiles publications content into
 * Drupal. You can use these importers from the Import page or directly at
 * /import/ucsf_profiles_publications
 */

/**
 * Implements hook_feeds_importer_default()
 */
function ucsf_profiles_feeds_importer_default() {
  $feeds_importers = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE;
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'ucsf_profiles_publications';
  $feeds_importer->config = array(
    'name' => 'Import UCSF Profiles publications',
    'description' => 'Import publications from the dynamically generated CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'source' => url('ucsf_profiles/download/csv',  array('absolute' => TRUE)),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => 'TAB',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'ucsf_profiles_publication',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'PublicationID',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'PublicationID',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'EmployeeID',
            'target' => 'field_pubs_employee_id',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Name',
            'target' => 'field_pubs_person:title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'ProfilesURL',
            'target' => 'field_pubs_person:url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Email',
            'target' => 'field_pubs_email',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Address1',
            'target' => 'field_pubs_address_1',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Address2',
            'target' => 'field_pubs_address_2',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Address3',
            'target' => 'field_pubs_address_3',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Address4',
            'target' => 'field_pubs_address_4',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Telephone',
            'target' => 'field_pubs_phone',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Fax',
            'target' => 'field_pubs_fax',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Department',
            'target' => 'field_pubs_department',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'School',
            'target' => 'field_pubs_school',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'PublicationTitle',
            'target' => 'field_pubs_pub_title',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'PublicationSourceName',
            'target' => 'field_pubs_pub_source:title',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'PublicationSourceURL',
            'target' => 'field_pubs_pub_source:url',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'PublicationAddedBy',
            'target' => 'field_pubs_pub_addedby',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'Weight',
            'target' => 'field_pubs_weight',
            'unique' => FALSE,
          ),
        ),
        'author' => '1',
        'authorize' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );
  $feeds_importers['ucsf_profiles_publications'] = $feeds_importer;

  return $feeds_importers;
}
