<?php
/**
 * @file
 * Generate a CSV file from previously downloaded JSON files.
 *
 * JSON files, stored in system files directory are read one by one to output
 * a dynamically generated CSV file.
 */

/**
 * Callback function to output CSV file with headers.
 *
 * @return string
 *    Outputs a CSV file.
 */
function ucsf_profiles_download_csv() {
  // Enable GZIP compression if available.
  if (!ob_start("ob_gzhandler")) { ob_start(); }

  // HTTP Headers.
  header("Content-type: application/csv; charset=utf-8");
  header("Content-Disposition: attachment; filename=publications-" . (int) microtime(true) . ".csv");
  header("Pragma: no-cache");
  header("Expires: 0");

  $output = '';

  $pubs_path = file_directory_path()
             . '/'
             . variable_get('ucsf_profiles_pubs_dir', 'ucsf_profiles_publications');

  $files = file_scan_directory($pubs_path, '.json');

  $rows = array();
  foreach ($files as $file) {
    $rows[$file->basename] = ucsf_files_json_to_csv($file->name, $file->filename);
  }

  // CSV headers.
  $output .= '"Weight"' .
    "\t" .'"PublicationID"' .
    "\t" .'"PublicationTitle"' .
    "\t" .'"PublicationSourceName"' .
    "\t" .'"PublicationSourceURL"' .
    "\t" .'"PublicationAddedBy"' .
    "\t" .'"EmployeeID"' .
    "\t" .'"Name"' .
    "\t" .'"ProfilesURL"' .
    "\t" .'"Email"' .
    "\t" .'"Address1"' .
    "\t" .'"Address2"' .
    "\t" .'"Address3"' .
    "\t" .'"Address4"' .
    "\t" .'"Telephone"' .
    "\t" .'"Fax"' .
    "\t" .'"Department"' .
    "\t" .'"School"' .
    "\n";

  // CSV rows.
  foreach ($rows as $row) {
    $output .= $row;
  }

  print $output;
}

/**
 * Converts JSON file to tab separated CSV file with no headers.
 *
 * @param int $employee_id
 *   Employee ID.
 *
 * @param string $filename
 *   The full path of JSON file to read contents from.
 *
 * @return string
 *   Tab separated CSV rows.
 */
function ucsf_files_json_to_csv($employee_id = 0, $filename = NULL) {
  $json = '';
  $publications = '';
  $output = '';

  if ($handle = fopen($filename, 'r')) {
    $json = stream_get_contents($handle);
    $json = json_decode($json, TRUE);
    fclose($handle);
    if (empty($json['Profiles'][0])) {
      return $output;
    }
    // empty address
    $json['Profiles'][0]['Address'] += array(
      'Address1' => '',
      'Address2' => '',
      'Address3' => '',
      'Address4' => '',
      'Telephone' => '',
      'Fax' => '',
    );
    // empty author
    $json['Profiles'][0] += array(
      'Name' => '',
      'ProfilesURL' => '',
      'Email' => '',
      'Department' => '',
      'School' => '',
    );

    // Author details.
    $author = '"' . $employee_id . '"' .
              "\t" . '"' . $json['Profiles'][0]['Name'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['ProfilesURL'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Email'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Address1'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Address2'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Address3'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Address4'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Telephone'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Address']['Fax'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['Department'] . '"' .
              "\t" . '"' . $json['Profiles'][0]['School'] . '"';

    // Publications array.
    $publications = $json['Profiles'][0]['Publications'];

    if (count($publications) > 0) {
      $weight = 1;
      $emptysource = array(
          'PublicationSourceName' => '',
          'PublicationSourceURL' => '',
          'PublicationAddedBy' => '',
      );
      foreach ($publications as $key => $publication) {
        if (empty($publication['PublicationID'])) {
          continue;
        }
        $publication += array(
          'PublicationID' => '',
          'PublicationTitle' => '',
        );
        if (empty($publication['PublicationSource'][0])) {
          $publication['PublicationSource'][0] = $emptysource;
        }
        else {
          $publication['PublicationSource'][0] += $emptysource;
        }
        $output .= '' .
                 '"' . $weight . '"' .
          "\t" . '"' . $publication['PublicationID'] . '"' .
          "\t" . '"' . $publication['PublicationTitle'] . '"' .
          "\t" . '"' . $publication['PublicationSource'][0]['PublicationSourceName'] . '"' .
          "\t" . '"' . $publication['PublicationSource'][0]['PublicationSourceURL'] . '"' .
          "\t" . '"' . $publication['PublicationSource'][0]['PublicationAddedBy'] . '"' .
          "\t" . $author .
          "\n";
        $weight++;
      }
    }
  }
  return $output;
}
