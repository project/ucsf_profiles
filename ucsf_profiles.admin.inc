<?php
/**
 * @file
 * Module administration settings.
 */

/**
 * Employee IDs form.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   The form structure.
 *
 * @see ucsf_profiles_admin_settings_submit()
 * @see ucsf_profiles_admin_settings_clear()
 * @ingroup forms
 */
function ucsf_profiles_admin_ids(&$form_state) {
  $form['ucsf_profiles'] = array(
    '#type' => 'fieldset',
    // '#title' => t('Settings'),
    '#description' => t(''),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['ucsf_profiles']['ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Employee IDs'),
    '#description' => t('Enter UCSF Employee IDs separated with commas or new lines.'),
    '#required' => FALSE,
    '#default_value' => variable_get('ucsf_profiles_ids', ''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => 'Reset',
    '#validate' => array('ucsf_profiles_admin_ids_clear'),
  );
  return $form;
}

/**
 * Form submission handler for ucsf_profiles_admin_ids().
 *
 * @param array $form
 *   The form being used to edit the module settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function ucsf_profiles_admin_ids_submit($form, &$form_state) {
  variable_set('ucsf_profiles_ids', $form_state['values']['ids']);
  drupal_set_message(t('Employee IDs are saved.'), 'status', FALSE);
}

/**
 * Form reset handler for ucsf_profiles_admin_ids().
 *
 * @param array $form
 *   The form being used to edit the module settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function ucsf_profiles_admin_ids_clear($form, &$form_state) {
  unset($form_state['values']);
  unset($form_state['storage']);
  $form_state['rebuild'] = TRUE;
  variable_set('ucsf_profiles_ids', '');
  drupal_set_message(t('Settings are reset to module defaults.'), 'warning', FALSE);
}

/**
 * Settings form.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   The form structure.
 *
 * @see ucsf_profiles_admin_settings_submit()
 * @see ucsf_profiles_admin_settings_clear()
 * @ingroup forms
 */
function ucsf_profiles_admin_settings(&$form_state) {
  $form['ucsf_profiles'] = array(
    '#type' => 'fieldset',
    // '#title' => t('Settings'),
    '#description' => t(''),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['ucsf_profiles']['dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Save directory'),
    '#default_value' => variable_get('ucsf_profiles_pubs_dir', 'ucsf_profiles_publications'),
    '#description' => t('A directory where the publication files will be stored within the %files system path.', array('%files' => file_directory_path())),
    '#required' => FALSE,
  );
  $form['ucsf_profiles']['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic downloads'),
    '#description' => '<p>' . t('Publications can be scheduled to be downloaded via cron.') . '</p>',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  if (variable_get('ucsf_profiles_cron_last', 0)) {
    $update_time = format_date(variable_get('ucsf_profiles_cron_last', 0));
  }
  else {
    $update_time = t('Never');
  }
  $form['ucsf_profiles']['update']['#description'] .= '<p>' . t('Last updated: @date', array('@date' => $update_time)). '</p>';

  $intervals = array(
    // 0.
    0,
    // 1-6, 9, 12 hours.
    3600, 3600 * 2, 3600 * 3, 3600 * 4, 3600 * 5, 3600 * 6, 3600 * 9, 3600 * 12,
    // 1-3 days.
    86400, 86400 * 2, 86400 * 3,
    // 1, 2, 3 weeks.
    604800, 604800 * 2, 604800 * 3,
  );
  $intervals = drupal_map_assoc($intervals, 'format_interval');
  $intervals[0] = t('Disabled');
  $form['ucsf_profiles']['update']['cron'] = array(
    '#type' => 'select',
    '#title' => t('Schedule'),
    '#default_value' => variable_get('ucsf_profiles_cron', 0),
    '#options' => $intervals,
    '#description' => t('Select how often publications be downloaded automatically. <strong>Note:</strong> This requires cron to run at least within this interval.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => 'Reset',
    '#validate' => array('ucsf_profiles_admin_settings_clear'),
  );
  return $form;
}

/**
 * Form submission handler for ucsf_profiles_admin_settings().
 *
 * @param array $form
 *   The form being used to edit the module settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function ucsf_profiles_admin_settings_submit($form, &$form_state) {
  variable_set('ucsf_profiles_pubs_dir', $form_state['values']['dir']);
  variable_set('ucsf_profiles_cron', $form_state['values']['cron']);
  drupal_set_message(t('Settings are saved.'), 'status', FALSE);
}

/**
 * Form reset handler for ucsf_profiles_admin_settings().
 *
 * @param array $form
 *   The form being used to edit the module settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function ucsf_profiles_admin_settings_clear($form, &$form_state) {
  unset($form_state['values']);
  unset($form_state['storage']);
  $form_state['rebuild'] = TRUE;
  variable_set('ucsf_profiles_pubs_dir', 'ucsf_profiles_publications');
  variable_set('ucsf_profiles_cron', 0);
  drupal_set_message(t('Settings are reset to module defaults.'), 'warning', FALSE);
}

/**
 * Download form.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   The form structure.
 *
 * @see ucsf_profiles_admin_download_submit()
 * @ingroup forms
 */
function ucsf_profiles_admin_download(&$form_state) {
  $form['ucsf_profiles']['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic updates'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  if (variable_get('ucsf_profiles_cron_last', 0)) {
    $update_time = format_date(variable_get('ucsf_profiles_cron_last', 0));
  }
  else {
    $update_time = t('Never');
  }
  $form['ucsf_profiles']['update']['#description'] = t('Last updated: @date', array('@date' => $update_time));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Download files from UCSF Profiles'),
  );

  if (trim(variable_get('ucsf_profiles_ids', '')) == '') {
    drupal_set_message(t('There are no valid Employee IDs defined yet.'), 'warning');
    $form['submit']['#disabled'] = TRUE;
  }
  return $form;
}

/**
 * Form submission handler for ucsf_profiles_admin_download().
 *
 * Triggers the function defining the batch operation.
 *
 * @param array $form
 *   The form being used to edit the module settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function ucsf_profiles_admin_download_submit($form, &$form_state) {
  module_load_include('inc', 'ucsf_profiles', 'ucsf_profiles.import');
  ucsf_profiles_batch();
}
